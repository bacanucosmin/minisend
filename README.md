# Install & usage

1. `docker-compose build`  
1. `docker-compose run api-workspace composer install`
1. `docker-compose run api-workspace php artisan key:generate`
1. `docker-compose run api-workspace php artisan migrate:fresh` if you want to run the tests or `docker-compose run api-workspace php artisan migrate:fresh --seed` if you want to seed the db with some data.
1. `docker-compose run client-workspace yarn`
1. `docker-compose up`

[Vue.js client](http://localhost:8080)  
[Laravel api](http://localhost)  
[Mailhog](http://localhost:8025)  
[minio (self-hosted s3)](http://localhost:9000) credentials: minio_access_key/minio_secret_key

## Architecture

`Keep in mind that your app could be used by thousands of clients`

A Vue.js client that can be hosted on any cdn and can be cached locally.  
A Laravel api to list/view/post emails. The email listing and view is cached in redis with a low ttl (5s) as a balance between db reads and up to date data.  
Emails are stored in the database.  
Attachments are stored in a "cloud" storage service (minio locally, s3 when deployed)  
When an email is posted, it is put on a queue in redis using Laravel's job functionality.   
A laravel worker (or more) reads from the queue and executes the job.  
The job sends a mail using the Laravel Mail module and a custom Mailable. The mailable builds the mail from the serialized Email and fetches the attachments from the cloud storage.  
If the mail is sent, the sent_at column is set to now().
If the mail failed while sending, the failed_at column is set to now().


---

We have a dedicated product for transactional emails - [www.mailersend.com](http://www.mailersend.com/)

MailerSend is a cloud-based transactional email service that allows our customers to manage transactional emails, create templates, ensures deliverability, and provide most needed analytic tools.

### Requirements

    We will ask you to create a mini version of a transactional email app, let's call it *MiniSend*, where a client could send an email through a Laravel API and also would have the ability to see how many emails have been sent, and track more interesting data about them through a VueJS frontend.

- Email API
    - Allow sending emails with these fields:
        - From (Sender)
        - To (Recipient)
        - Subject
        - Text content
        - HTML content
        - Attachment(-s)
- A frontend
    - A list of email activities
        - Search by sender, recipient, subject
        - Possible statuses:
            - Posted
            - Sent
            - Failed
        - A view of single email
        - A list of emails for a recipient

### Notes

- Both API & frontend auth are not a must
- You can send emails just by using PHP `mail` function
- Keep in mind that your app could be used by thousands of clients
- You are welcome to add additional fields to the email API & the frontend
