docker run --rm --interactive --tty \
  --workdir /app \
  --volume $PWD/minisend-client:/app \
  node /bin/bash -c "yarn"