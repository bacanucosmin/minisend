<?php

namespace Database\Factories;

use App\Models\Email;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Email::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sender_name' => $this->faker->name,
            'sender_email'  => $this->faker->safeEmail,
            'recipient_name'  => $this->faker->name,
            'recipient_email'  => $this->faker->safeEmail,
            'subject'  => $this->faker->text(),
            'text_content'  => $this->faker->text,
            'html_content' => $this->faker->randomHtml(),
            'posted_at' => Carbon::now(),
            'attachments' => [],
        ];
    }
}
