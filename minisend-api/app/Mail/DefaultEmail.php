<?php

namespace App\Mail;

use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class DefaultEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Email
     */
    private $email;

    /**
     * Create a new message instance.
     *
     * @param Email $email
     */
    public function __construct(Email $email)
    {
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail =  $this->html($this->email->html_content)
            ->subject($this->email->subject)
            ->text('preview.html', ['content' => $this->email->text_content])
            ->from($this->email->sender_email, $this->email->sender_name)
            ->to($this->email->recipient_email, $this->email->recipient_name)
            ;

        foreach ($this->email->attachments as $fileName) {
            $file = Storage::disk('s3')->get('/' . $this->email->id . '/' . $fileName);
            $mail->attachData($file, $fileName);
        }

        return $mail;
    }

}
