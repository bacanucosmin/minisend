<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmailPostRequest;
use App\Jobs\SendEmail;
use App\Models\Email;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class EmailsController extends Controller
{
    /**
     * @param Request $request
     * @return Email[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        $query = Email::search(
            $request->get('sender'),
            $request->get('recipient'),
            $request->get('subject'),
        );

        //don't cache results if the request is for a recipient listing
        if ($recipientEmail = $request->get('recipient_email_exact')) {
            $query->where('recipient_email', $recipientEmail);

            return $query->paginate();
        }


        //cache public listings for 5 seconds
        return Cache::remember($request->fullUrlWithQuery($request->only(['sender', 'recipient', 'subject'])), 5, function() use ($query) {
            return $query->paginate();
        });
    }

    /**
     * @param EmailPostRequest $request
     * @return mixed
     */
    public function store(EmailPostRequest $request)
    {
        $email = new Email();
        $data = $request->only([
            'sender_name',
            'sender_email',
            'recipient_name',
            'recipient_email',
            'subject',
            'text_content',
            'text_content',
            'html_content',
        ]);
        $data['attachments'] = [];

        $email->forceFill($data);
        $email->changeStatusToPosted();


        if ($request->hasFile('attachments')) {
            $email->addAttachments($request->attachments);
        }

        dispatch(new SendEmail($email));

        $email->fresh();

        return response($email, 201);
    }

    /**
     * @param Request $request
     * @param Email $email
     * @return Email
     */
    public function show(Request $request, Email $email)
    {
        //cache email for 5 seconds
        return Cache::remember($request->fullUrl(), 5, function() use ($email) {
            return $email;
        });
    }
}
