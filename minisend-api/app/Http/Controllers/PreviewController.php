<?php

namespace App\Http\Controllers;

use App\Models\Email;

class PreviewController extends Controller
{
    public function html(Email $email)
    {
        return view('preview.html', ['content' => $email->html_content]);
    }
}
