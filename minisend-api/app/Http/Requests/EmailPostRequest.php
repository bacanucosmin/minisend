<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required',
            'text_content' => 'required_without:html_content',
            'html_content' => 'required_without:text_content',
            'sender_name' => '',
            'sender_email' => 'required',
            'recipient_name' => '',
            'recipient_email' => 'required',
            'attachments.*' => 'file',
        ];
    }
}
