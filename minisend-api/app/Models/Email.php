<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;

class Email extends Model
{
    use HasFactory;

    const STATUS_POSTED = 0;
    const STATUS_SENT = 1;
    const STATUS_FAILED = 2;

    protected $fillable = [
        'sender_name',
        'sender_email',
        'recipient_name',
        'recipient_email',
        'subject',
        'text_content',
        'html_content',
    ];

    protected $appends = [
        'status',
        'status_changed_at',
        'status_changed_at_readable',
        'html_preview_url',
    ];

    protected $dates = [
        'sent_at',
        'failed_at',
        'posted_at',
        'status_changed_at',
    ];

    protected $casts = [
        'attachments' => 'array'
    ];

    /**
     * Used to filters the result by partial/full match against sender and recipient fields and subject
     *
     * @param $query
     * @param $sender
     * @param $recipient
     * @param $subject
     */
    public function scopeSearch($query, $sender, $recipient, $subject)
    {
        if ($sender) {
            $query->where(function (Builder $q) use ($sender) {
                $q->orWhere('sender_name', 'LIKE', '%' . $sender . '%')
                    ->orWhere('sender_email', 'LIKE', '%' . $sender . '%');

            });
        }

        if ($recipient) {
            $query->where(function (Builder $q) use ($recipient) {

                $q->orWhere('recipient_name', 'LIKE', '%' . $recipient . '%')
                    ->orWhere('recipient_email', 'LIKE', '%' . $recipient . '%');

            });
        }

        if ($subject) {
            $query->where('subject', 'LIKE', '%' . $subject . '%');
        }
    }


    /**
     * The status string based on the status dates
     *
     * @return string
     * @throws \Exception
     */
    public function getStatusAttribute(): string
    {
        if($this->sent_at) {
            return 'Sent';
        }

        if($this->failed_at) {
            return 'Failed';
        }

        if($this->posted_at) {
            return 'Posted';
        }

        throw new \Exception('Could not determine status for email.');
    }

    /**
     * The date of the last status change
     *
     * @throws \Exception
     */
    public function getStatusChangedAtAttribute()
    {
        if($this->sent_at) {
            return $this->sent_at;
        }

        if($this->failed_at) {
            return $this->failed_at;
        }

        if($this->posted_at) {
            return $this->posted_at;
        }

        throw new \Exception('Could not determine status for email.');
    }

    /**
     * The last status change as a readable string (x time ago)
     * @return mixed
     */
    public function getStatusChangedAtReadableAttribute()
    {
        return $this->status_changed_at->diffForHumans();
    }

    /**
     * An url where the html content can be previewed
     * @return string
     */
    public function getHtmlPreviewUrlAttribute()
    {
        return route('preview.html', $this->id);
    }

    public function changeStatusToPosted()
    {
        $this->forceFill(['posted_at' => now()])->save();
    }

    public function changeStatusToSent()
    {
        $this->forceFill(['sent_at' => now()])->save();
    }

    public function changeStatusToFailed()
    {
        $this->forceFill(['failed_at' => now()])->save();
    }

    /**
     * @param UploadedFile[] $uploadedFiles
     */
    public function addAttachments(array $uploadedFiles)
    {
        $attachments = [];

        foreach ($uploadedFiles as $uploadedFile) {
            $name = $uploadedFile->getClientOriginalName();
            $uploadedFile->storeAs(
                '/' . $this->id,
                $name,
                ['disk' => 's3']
            );
            $attachments[] = $name;
        }

        $this->forceFill(['attachments' => $attachments])->save();
    }
}
