<?php

namespace App\Jobs;

use App\Mail\DefaultEmail;
use App\Models\Email;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Email
     */
    private $email;

    /**
     * Create a new job instance.
     *
     * @param Email $email
     */
    public function __construct(Email $email)
    {

        $this->email = $email;
    }

    public function handle()
    {
        try {
            Mail::send(new DefaultEmail($this->email));
            $this->email->changeStatusToSent();
        } catch (\Exception $e) {
            $this->email->changeStatusToSent();
            throw $e;
        }
    }
}
