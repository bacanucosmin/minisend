<?php

namespace Tests\Feature;

use App\Models\Email;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class EmailsControllerTest extends TestCase
{
    use DatabaseTransactions;

    public function testListingTheEmailsWithoutSearchingOrFiltering()
    {
        $email = Email::factory()->create();

        $response = $this->getJson(route('emails.index'));

        $response->assertJson(['data' => [['id' => $email->id, 'sender_email' => $email->sender_email]]]);

    }

    private function setupEmails()
    {
        Email::factory()->create([
            'sender_name' => '-sender_name-',
            'sender_email' => '-sender_email-',
            'recipient_name' => '-recipient_name-',
            'recipient_email' => '-recipient_email-',
            'subject' => '-subject-'
        ]);

        Email::factory()->create([
            'sender_name' => '-sender_name-',
        ]);

        Email::factory()->create([
            'recipient_email' => '-sender_name-',
        ]);

        Email::factory()->create([
            'recipient_email' => '-recipient_email-',
        ]);

        Email::factory()->create([
            'subject' => '-sender_name-'
        ]);
    }

    public function testEmailSearchAddsPresentTerms()
    {
        $this->setupEmails();

        $response = $this->getJson(route('emails.index', [
            'sender' => 'sender_name',
            'recipient' => 'recipient_email',
            'subject' => 'subject',
        ]));

        $response->assertJson(['data' => [['sender_name' => '-sender_name-']]])
            ->assertJsonCount(1, 'data');

    }
    public function testEmailSearchIgnoresMissingOrEmptyTerms()
    {
        $this->setupEmails();

        $response = $this->getJson(route('emails.index', [
            'sender' => 'sender_name',
            'recipient' => '',
            'subject' => '',
        ]));

        $response->assertJson(['data' => [['sender_name' => '-sender_name-']]])
            ->assertJsonCount(2, 'data');

    }

    public function testEmailListCanBeFilteredByExactRecipientEmail()
    {
        $this->setupEmails();

        //0 expected result count because the exact match is `-recipient_email-`, not `recipient_email`
        $response = $this->getJson(route('emails.index', [
            'recipient_email_exact' => 'recipient_email',
        ]));

        $response->assertJsonCount(0, 'data');

        $response = $this->getJson(route('emails.index', [
            'recipient_email_exact' => '-recipient_email-',
        ]));

        $response->assertJsonCount(2, 'data');
    }

    public function testEmailSearchWithFilter()
    {
        $this->setupEmails();

        //there are 2 emails with -recipient_email-
        //and only 1 with subject -subject-
        $response = $this->getJson(route('emails.index', [
            'recipient_email_exact' => '-recipient_email-',
            'subject' => 'subject'
        ]));

        $response->assertJsonCount(1, 'data');
    }

    public function testPostingAnEmail()
    {
        $email = Email::factory()->create();

        $response = $this->postJson(route('emails.store'), $email->toArray());

        $response->assertJsonMissingValidationErrors();
        $response->assertCreated();

        $this->assertDatabaseHas('emails', ['sender_email' => $email->sender_email]);
    }
}
