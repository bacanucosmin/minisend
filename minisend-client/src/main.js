import Vue from 'vue'
import VueRouter from 'vue-router'

import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.css'
import ListEmails from "@/views/ListEmails";
import PostEmail from "@/views/PostEmail";
import ViewEmail from "@/views/ViewEmail";
import ListEmailsForRecipient from "@/views/ListEmailsForRecipient";

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
  { path: '/post', component: PostEmail },
  { path: '/view/:emailId', component: ViewEmail },
  { path: '/forRecipient/:recipientEmail', component: ListEmailsForRecipient },
  { path: '/', component: ListEmails },
]

const router = new VueRouter({
  linkActiveClass: 'active',
  routes // short for `routes: routes`
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
